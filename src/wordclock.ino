// #define DEBUG

#include <ArduinoOTA.h>
#include <DebugUtils.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <FastLED.h>
#include <MatrixLEDPositionsDef.h>
#include <NTPClient.h>
#include <Timezone.h>
#include <WiFiUdp.h>

#define NUM_LEDS 121
#define DATA_PIN D4

/**********************
 * SET WIFI CREDENTIALS
 **********************/
std::vector<std::string> ssids         = {
  "xxx"
  };
std::vector<std::string> wifiPasswords = {
  "xxx"
  };
const char*              hostname      = "wordclock";

enum Mode { wordclock, sparkly };
Mode mode = Mode::sparkly;

typedef struct {
  int r;
  int g;
  int b;
} color_t;

CRGB   leds[NUM_LEDS];
int    r  = 255;
int    g  = 255;
int    b  = 255;
String ip = "";

WiFiUDP ntpUDP;

TimeChangeRule deEDT = {"CET", Last, Sun, Mar, 3, 60};   // UTC + 1 hours
TimeChangeRule deEST = {"CEST", Last, Sun, Oct, 3, 120}; // UTC + 2 hours
Timezone       timezoneDeBerlin(deEST, deEDT);

NTPClient timeClient(ntpUDP, "europe.pool.ntp.org");

ESP8266WiFiMulti wifiMulti;
boolean          connectioWasAlive = true;

ESP8266WebServer server(80);
String           header;

typedef struct {
  int      hour;
  int      minute;
  Timezone tz;
} current_time_t;

color_t        bgColor;
color_t        fgColor;
current_time_t currentTime{-1, -1, timezoneDeBerlin};
int            startupAnimationLED   = 0;
const long     sparklyUpdateInterval = 50;
int            sparklyDelay          = 1;
long           previousTime          = 0;
int            latestHour            = -1;
int            latestMinute          = -1;

color_t hexToRgb(String value) {
  value.replace("#", "");
  int number = (int)strtol(value.c_str(), NULL, 16);

  // Split them up into r, g, b values
  int r = number >> 16;
  int g = number >> 8 & 0xFF;
  int b = number & 0xFF;

  color_t rgb;
  rgb.r = r;
  rgb.g = g;
  rgb.b = b;

  return rgb;
}

String rgbToHex(const color_t hex) {
  long hexColor = ((long)hex.r << 16L) | ((long)hex.g << 8L) | (long)hex.b;

  String out = String(hexColor, HEX);

  while (out.length() < 6) {
    out = "0" + out;
  }

  return out;
}

void setTime(int hour, int minute) {
  FastLED.clear();

  if (hour == -1 || minute == -1) {
    return;
  }

  minute = (minute - (minute % 5));

  if (minute >= 25) {
    hour += 1;
  }

  minute = minute / 5;
  hour   = hour % 12;

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(bgColor.r * 0.2, bgColor.g * 0.2, bgColor.b * 0.2);
  }

  for (int i = 0; i < 5; i++) {
    leds[time_it_is[i]].setRGB(fgColor.r, fgColor.g, fgColor.b);
  }

  for (int m = 0; m < 12; m++) {
    if (time_minutes[minute][m] >= 0) {
      leds[time_minutes[minute][m]].setRGB(fgColor.r, fgColor.g, fgColor.b);
    }
  }

  for (int h = 0; h < 6; h++) {
    if (time_hours[hour][h] >= 0) {
      leds[time_hours[hour][h]].setRGB(fgColor.r, fgColor.g, fgColor.b);
    }
  }

  FastLED.show();
}

void updateSparkly() {
  if (sparklyDelay++ == 10) {
    leds[random(0, NUM_LEDS - 1)].setRGB(255, 255, 255);
    sparklyDelay = 1;
  }
  FastLED.show();
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].subtractFromRGB(1);
  }
}

String getTimeForm() {
  String content = "";

  content += "<div>";
  content += "<label>Foreground color</label>";
  content += "<input name=\"fg\" value=\"#" + rgbToHex(fgColor) + "\" type=\"color\">";
  content += "</div>";
  content += "<div>";
  content += "<label>Background color</label>";
  content += "<input name=\"bg\" value=\"#" + rgbToHex(bgColor) + "\" type=\"color\">";
  content += "</div>";

  return content;
}

String htmlOption(String label, String value, String store) {
  String content = "<option value=\"" + value + "\"";

  if (store == value) {
    content += " selected=\"selected\"";
  }

  content += ">" + label + "</option>";

  return content;
}

void change() {
  bool change = false;

  if (server.hasArg("fg")) {
    fgColor = hexToRgb(server.arg("fg"));
    change  = true;
  }

  if (server.hasArg("bg")) {
    bgColor = hexToRgb(server.arg("bg"));
    change  = true;
  }

  if (change == true) {
    show();
  }
}

void handleRootPath() {
  String content = "";

  change();

  content += "<!DOCTYPE html><html>";
  content += "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  content += "<style>";
  content += "* { box-sizing: border-box; }";
  content += "html, body { font-family: Helvetica; margin: 0; padding: 0; }";
  content += ".form { margin: auto; max-width: 400px; }";
  content += ".form div { margin: 0; padding: 20px 0; width: 100%; font-size: 1.4rem; }";
  content += "label { width: 60%; display: inline-block; margin: 0; vertical-align: middle; }";
  content += "input, select { width: 38%; display: inline-block; margin: 0; border: 1px solid "
             "#eee; padding: 0; height: 40px; vertical-align: middle; }";
  content +=
      "button { display: inline-block; width: 100%; font-size: 1.4rem; background-color: green; "
      "border: 1px solid #eee; color: #fff; padding-top: 10px; padding-bottom: 10px; }";
  content += "</style>";
  content += "</head>";
  content += "<body>";

  content += "<h1>WordClock Einstellungen</h1>";
  content += "<form class=\"form\" method=\"post\" action=\"\">";
  content += getTimeForm();
  content += "<div>";
  content += "<button type=\"submit\">Anwenden</button>";
  content += "</div>";
  content += "</form>";
  content += "</body></html>";

  // server.sendHeader("Location", "http://" + ip);
  server.send(200, "text/html", content);
}

void show() { setTime(latestHour, latestMinute); }

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  Serial.setDebugOutput(true);
  delay(1000);
#endif

  bgColor.r = 0;
  bgColor.g = 0;
  bgColor.b = 0;

  fgColor.r = 255;
  fgColor.g = 0;
  fgColor.b = 0;

  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(50);

  for (int i = 0; i < ssids.size(); i++) {
    wifiMulti.addAP(ssids[i].c_str(), wifiPasswords[i].c_str());
    DEBUG_PRINTLN("WIFI AP added:");
    DEBUG_PRINTLN(ssids[i].c_str());
    DEBUG_PRINTLN(wifiPasswords[i].c_str());
  }
  WiFi.hostname(hostname);

  delay(500);

  server.on("/", handleRootPath);
  server.begin();

  timeClient.begin();

  ArduinoOTA.begin();

  FastLED.clear();
  FastLED.show();
}

void loop() {
  wifiMulti.run();
  if (WiFi.status() == WL_CONNECTED && latestHour > -1 && mode != Mode::wordclock) {
    mode         = Mode::wordclock;
    previousTime = 0;
    FastLED.clear();

    DEBUG_PRINTLN("");
    DEBUG_PRINTLN("WiFi connected.");
    DEBUG_PRINTLN("IP address: ");
    DEBUG_PRINTLN(WiFi.localIP());
    DEBUG_PRINTLN("SSID: ");
    DEBUG_PRINTLN(WiFi.SSID().c_str());
  }

  unsigned long currentTime = millis();
  unsigned long elapsedTime = currentTime - previousTime;

  ArduinoOTA.handle();

  if (timezoneDeBerlin.locIsDST(now())) {
    timeClient.setTimeOffset(3600);
  } else {
    timeClient.setTimeOffset(3600 * 2);
  }

  timeClient.update();
  setTime(timeClient.getEpochTime());

  int h = timeClient.getHours();
  int m = timeClient.getMinutes();

  if (h != latestHour || m != latestMinute) {
    latestHour   = h;
    latestMinute = m;
    if (mode == Mode::wordclock) {
      show();
    }
  }

  if (mode == Mode::sparkly) {
    if (elapsedTime > sparklyUpdateInterval) {
      previousTime = currentTime;
      updateSparkly();
    }
  }

  server.handleClient();
}
